import { browser, ExpectedConditions as ec } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ACMETransactionComponentsPage, ACMETransactionUpdatePage } from './acme-transaction.page-object';

describe('ACMETransaction e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let aCMETransactionUpdatePage: ACMETransactionUpdatePage;
    let aCMETransactionComponentsPage: ACMETransactionComponentsPage;

    beforeAll(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load ACMETransactions', async () => {
        await navBarPage.goToEntity('acme-transaction');
        aCMETransactionComponentsPage = new ACMETransactionComponentsPage();
        expect(await aCMETransactionComponentsPage.getTitle()).toMatch(/ACME Transactions/);
    });

    it('should load create ACMETransaction page', async () => {
        await aCMETransactionComponentsPage.clickOnCreateButton();
        aCMETransactionUpdatePage = new ACMETransactionUpdatePage();
        expect(await aCMETransactionUpdatePage.getPageTitle()).toMatch(/Create or edit a ACME Transaction/);
        await aCMETransactionUpdatePage.cancel();
    });

    it('should create and save ACMETransactions', async () => {
        await aCMETransactionComponentsPage.clickOnCreateButton();
        await aCMETransactionUpdatePage.setAccountNumberInput('accountNumber');
        expect(await aCMETransactionUpdatePage.getAccountNumberInput()).toMatch('accountNumber');
        await aCMETransactionUpdatePage.setTransactionDateInput('2000-12-31');
        expect(await aCMETransactionUpdatePage.getTransactionDateInput()).toMatch('2000-12-31');
        await aCMETransactionUpdatePage.setAmountInput('5');
        expect(await aCMETransactionUpdatePage.getAmountInput()).toMatch('5');
        await aCMETransactionUpdatePage.setTransactionRemarkInput('transactionRemark');
        expect(await aCMETransactionUpdatePage.getTransactionRemarkInput()).toMatch('transactionRemark');
        await aCMETransactionUpdatePage.transactionTypeSelectLastOption();
        await aCMETransactionUpdatePage.setAvailableBalanceInput('5');
        expect(await aCMETransactionUpdatePage.getAvailableBalanceInput()).toMatch('5');
        await aCMETransactionUpdatePage.save();
        expect(await aCMETransactionUpdatePage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(async () => {
        await navBarPage.autoSignOut();
    });
});
