import { element, by, ElementFinder } from 'protractor';

export class ACMETransactionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    title = element.all(by.css('jhi-acme-transaction div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ACMETransactionUpdatePage {
    pageTitle = element(by.id('jhi-acme-transaction-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    accountNumberInput = element(by.id('field_accountNumber'));
    transactionDateInput = element(by.id('field_transactionDate'));
    amountInput = element(by.id('field_amount'));
    transactionRemarkInput = element(by.id('field_transactionRemark'));
    transactionTypeSelect = element(by.id('field_transactionType'));
    availableBalanceInput = element(by.id('field_availableBalance'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setAccountNumberInput(accountNumber) {
        await this.accountNumberInput.sendKeys(accountNumber);
    }

    async getAccountNumberInput() {
        return this.accountNumberInput.getAttribute('value');
    }

    async setTransactionDateInput(transactionDate) {
        await this.transactionDateInput.sendKeys(transactionDate);
    }

    async getTransactionDateInput() {
        return this.transactionDateInput.getAttribute('value');
    }

    async setAmountInput(amount) {
        await this.amountInput.sendKeys(amount);
    }

    async getAmountInput() {
        return this.amountInput.getAttribute('value');
    }

    async setTransactionRemarkInput(transactionRemark) {
        await this.transactionRemarkInput.sendKeys(transactionRemark);
    }

    async getTransactionRemarkInput() {
        return this.transactionRemarkInput.getAttribute('value');
    }

    async setTransactionTypeSelect(transactionType) {
        await this.transactionTypeSelect.sendKeys(transactionType);
    }

    async getTransactionTypeSelect() {
        return this.transactionTypeSelect.element(by.css('option:checked')).getText();
    }

    async transactionTypeSelectLastOption() {
        await this.transactionTypeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setAvailableBalanceInput(availableBalance) {
        await this.availableBalanceInput.sendKeys(availableBalance);
    }

    async getAvailableBalanceInput() {
        return this.availableBalanceInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}
