import { browser, ExpectedConditions as ec } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ACMEBankAccountComponentsPage, ACMEBankAccountUpdatePage } from './acme-bank-account.page-object';

describe('ACMEBankAccount e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let aCMEBankAccountUpdatePage: ACMEBankAccountUpdatePage;
    let aCMEBankAccountComponentsPage: ACMEBankAccountComponentsPage;

    beforeAll(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load ACMEBankAccounts', async () => {
        await navBarPage.goToEntity('acme-bank-account');
        aCMEBankAccountComponentsPage = new ACMEBankAccountComponentsPage();
        expect(await aCMEBankAccountComponentsPage.getTitle()).toMatch(/ACME Bank Accounts/);
    });

    it('should load create ACMEBankAccount page', async () => {
        await aCMEBankAccountComponentsPage.clickOnCreateButton();
        aCMEBankAccountUpdatePage = new ACMEBankAccountUpdatePage();
        expect(await aCMEBankAccountUpdatePage.getPageTitle()).toMatch(/Create or edit a ACME Bank Account/);
        await aCMEBankAccountUpdatePage.cancel();
    });

    it('should create and save ACMEBankAccounts', async () => {
        await aCMEBankAccountComponentsPage.clickOnCreateButton();
        await aCMEBankAccountUpdatePage.setAccountNumberInput('accountNumber');
        expect(await aCMEBankAccountUpdatePage.getAccountNumberInput()).toMatch('accountNumber');
        await aCMEBankAccountUpdatePage.setUsernameInput('username');
        expect(await aCMEBankAccountUpdatePage.getUsernameInput()).toMatch('username');
        await aCMEBankAccountUpdatePage.setPasswordInput('password');
        expect(await aCMEBankAccountUpdatePage.getPasswordInput()).toMatch('password');
        await aCMEBankAccountUpdatePage.setFirstNameInput('firstName');
        expect(await aCMEBankAccountUpdatePage.getFirstNameInput()).toMatch('firstName');
        await aCMEBankAccountUpdatePage.setLastNameInput('lastName');
        expect(await aCMEBankAccountUpdatePage.getLastNameInput()).toMatch('lastName');
        await aCMEBankAccountUpdatePage.setEmailInput('email');
        expect(await aCMEBankAccountUpdatePage.getEmailInput()).toMatch('email');
        await aCMEBankAccountUpdatePage.setPhoneInput('phone');
        expect(await aCMEBankAccountUpdatePage.getPhoneInput()).toMatch('phone');
        await aCMEBankAccountUpdatePage.setAddressInput('address');
        expect(await aCMEBankAccountUpdatePage.getAddressInput()).toMatch('address');
        await aCMEBankAccountUpdatePage.setPostalCodeInput('postalCode');
        expect(await aCMEBankAccountUpdatePage.getPostalCodeInput()).toMatch('postalCode');
        await aCMEBankAccountUpdatePage.setPanNumberInput('panNumber');
        expect(await aCMEBankAccountUpdatePage.getPanNumberInput()).toMatch('panNumber');
        await aCMEBankAccountUpdatePage.save();
        expect(await aCMEBankAccountUpdatePage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(async () => {
        await navBarPage.autoSignOut();
    });
});
