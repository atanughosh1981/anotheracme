/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AcmebankTestModule } from '../../../test.module';
import { ACMETransactionDetailComponent } from 'app/entities/acme-transaction/acme-transaction-detail.component';
import { ACMETransaction } from 'app/shared/model/acme-transaction.model';

describe('Component Tests', () => {
    describe('ACMETransaction Management Detail Component', () => {
        let comp: ACMETransactionDetailComponent;
        let fixture: ComponentFixture<ACMETransactionDetailComponent>;
        const route = ({ data: of({ aCMETransaction: new ACMETransaction(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AcmebankTestModule],
                declarations: [ACMETransactionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ACMETransactionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ACMETransactionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.aCMETransaction).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
