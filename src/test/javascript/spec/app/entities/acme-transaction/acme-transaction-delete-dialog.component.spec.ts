/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AcmebankTestModule } from '../../../test.module';
import { ACMETransactionDeleteDialogComponent } from 'app/entities/acme-transaction/acme-transaction-delete-dialog.component';
import { ACMETransactionService } from 'app/entities/acme-transaction/acme-transaction.service';

describe('Component Tests', () => {
    describe('ACMETransaction Management Delete Component', () => {
        let comp: ACMETransactionDeleteDialogComponent;
        let fixture: ComponentFixture<ACMETransactionDeleteDialogComponent>;
        let service: ACMETransactionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AcmebankTestModule],
                declarations: [ACMETransactionDeleteDialogComponent]
            })
                .overrideTemplate(ACMETransactionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ACMETransactionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ACMETransactionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
