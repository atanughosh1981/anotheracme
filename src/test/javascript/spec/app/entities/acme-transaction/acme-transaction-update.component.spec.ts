/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AcmebankTestModule } from '../../../test.module';
import { ACMETransactionUpdateComponent } from 'app/entities/acme-transaction/acme-transaction-update.component';
import { ACMETransactionService } from 'app/entities/acme-transaction/acme-transaction.service';
import { ACMETransaction } from 'app/shared/model/acme-transaction.model';

describe('Component Tests', () => {
    describe('ACMETransaction Management Update Component', () => {
        let comp: ACMETransactionUpdateComponent;
        let fixture: ComponentFixture<ACMETransactionUpdateComponent>;
        let service: ACMETransactionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AcmebankTestModule],
                declarations: [ACMETransactionUpdateComponent]
            })
                .overrideTemplate(ACMETransactionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ACMETransactionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ACMETransactionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ACMETransaction(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.aCMETransaction = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ACMETransaction();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.aCMETransaction = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
