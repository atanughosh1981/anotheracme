package com.mck.acmebank.account.domain.enumeration;

/**
 * The TRANSTYPE enumeration.
 */
public enum TRANSTYPE {
    DEBIT, CREDIT
}
