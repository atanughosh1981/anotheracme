/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.acmebank.account.web.rest.vm;
