package com.mck.acmebank.account.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mck.acmebank.account.domain.ACMETransaction;
import com.mck.acmebank.account.repository.ACMETransactionRepository;
import com.mck.acmebank.account.web.rest.errors.BadRequestAlertException;
import com.mck.acmebank.account.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ACMETransaction.
 */
@RestController
@RequestMapping("/api")
public class ACMETransactionResource {

    private final Logger log = LoggerFactory.getLogger(ACMETransactionResource.class);

    private static final String ENTITY_NAME = "aCMETransaction";

    private final ACMETransactionRepository aCMETransactionRepository;

    public ACMETransactionResource(ACMETransactionRepository aCMETransactionRepository) {
        this.aCMETransactionRepository = aCMETransactionRepository;
    }

    /**
     * POST  /acme-transactions : Create a new aCMETransaction.
     *
     * @param aCMETransaction the aCMETransaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new aCMETransaction, or with status 400 (Bad Request) if the aCMETransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/acme-transactions")
    @Timed
    public ResponseEntity<ACMETransaction> createACMETransaction(@Valid @RequestBody ACMETransaction aCMETransaction) throws URISyntaxException {
        log.debug("REST request to save ACMETransaction : {}", aCMETransaction);
        if (aCMETransaction.getId() != null) {
            throw new BadRequestAlertException("A new aCMETransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ACMETransaction result = aCMETransactionRepository.save(aCMETransaction);
        return ResponseEntity.created(new URI("/api/acme-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /acme-transactions : Updates an existing aCMETransaction.
     *
     * @param aCMETransaction the aCMETransaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated aCMETransaction,
     * or with status 400 (Bad Request) if the aCMETransaction is not valid,
     * or with status 500 (Internal Server Error) if the aCMETransaction couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/acme-transactions")
    @Timed
    public ResponseEntity<ACMETransaction> updateACMETransaction(@Valid @RequestBody ACMETransaction aCMETransaction) throws URISyntaxException {
        log.debug("REST request to update ACMETransaction : {}", aCMETransaction);
        if (aCMETransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ACMETransaction result = aCMETransactionRepository.save(aCMETransaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aCMETransaction.getId().toString()))
            .body(result);
    }

    /**
     * GET  /acme-transactions : get all the aCMETransactions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of aCMETransactions in body
     */
    @GetMapping("/acme-transactions")
    @Timed
    public List<ACMETransaction> getAllACMETransactions() {
        log.debug("REST request to get all ACMETransactions");
        return aCMETransactionRepository.findAll();
    }

    /**
     * GET  /acme-transactions/:id : get the "id" aCMETransaction.
     *
     * @param id the id of the aCMETransaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aCMETransaction, or with status 404 (Not Found)
     */
    @GetMapping("/acme-transactions/{id}")
    @Timed
    public ResponseEntity<ACMETransaction> getACMETransaction(@PathVariable Long id) {
        log.debug("REST request to get ACMETransaction : {}", id);
        Optional<ACMETransaction> aCMETransaction = aCMETransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(aCMETransaction);
    }

    /**
     * DELETE  /acme-transactions/:id : delete the "id" aCMETransaction.
     *
     * @param id the id of the aCMETransaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/acme-transactions/{id}")
    @Timed
    public ResponseEntity<Void> deleteACMETransaction(@PathVariable Long id) {
        log.debug("REST request to delete ACMETransaction : {}", id);

        aCMETransactionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
