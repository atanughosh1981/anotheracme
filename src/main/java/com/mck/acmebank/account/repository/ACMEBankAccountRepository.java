package com.mck.acmebank.account.repository;

import com.mck.acmebank.account.domain.ACMEBankAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ACMEBankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ACMEBankAccountRepository extends JpaRepository<ACMEBankAccount, Long> {

}
