import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AcmebankSharedModule } from 'app/shared';
import {
    ACMEBankAccountComponent,
    ACMEBankAccountDetailComponent,
    ACMEBankAccountUpdateComponent,
    ACMEBankAccountDeletePopupComponent,
    ACMEBankAccountDeleteDialogComponent,
    aCMEBankAccountRoute,
    aCMEBankAccountPopupRoute
} from './';

const ENTITY_STATES = [...aCMEBankAccountRoute, ...aCMEBankAccountPopupRoute];

@NgModule({
    imports: [AcmebankSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ACMEBankAccountComponent,
        ACMEBankAccountDetailComponent,
        ACMEBankAccountUpdateComponent,
        ACMEBankAccountDeleteDialogComponent,
        ACMEBankAccountDeletePopupComponent
    ],
    entryComponents: [
        ACMEBankAccountComponent,
        ACMEBankAccountUpdateComponent,
        ACMEBankAccountDeleteDialogComponent,
        ACMEBankAccountDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AcmebankACMEBankAccountModule {}
