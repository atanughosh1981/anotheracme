import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IACMEBankAccount } from 'app/shared/model/acme-bank-account.model';
import { Principal } from 'app/core';
import { ACMEBankAccountService } from './acme-bank-account.service';

@Component({
    selector: 'jhi-acme-bank-account',
    templateUrl: './acme-bank-account.component.html'
})
export class ACMEBankAccountComponent implements OnInit, OnDestroy {
    aCMEBankAccounts: IACMEBankAccount[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private aCMEBankAccountService: ACMEBankAccountService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.aCMEBankAccountService.query().subscribe(
            (res: HttpResponse<IACMEBankAccount[]>) => {
                this.aCMEBankAccounts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInACMEBankAccounts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IACMEBankAccount) {
        return item.id;
    }

    registerChangeInACMEBankAccounts() {
        this.eventSubscriber = this.eventManager.subscribe('aCMEBankAccountListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
