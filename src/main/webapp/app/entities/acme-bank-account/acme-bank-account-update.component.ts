import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IACMEBankAccount } from 'app/shared/model/acme-bank-account.model';
import { ACMEBankAccountService } from './acme-bank-account.service';

@Component({
    selector: 'jhi-acme-bank-account-update',
    templateUrl: './acme-bank-account-update.component.html'
})
export class ACMEBankAccountUpdateComponent implements OnInit {
    private _aCMEBankAccount: IACMEBankAccount;
    isSaving: boolean;

    constructor(private aCMEBankAccountService: ACMEBankAccountService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ aCMEBankAccount }) => {
            this.aCMEBankAccount = aCMEBankAccount;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.aCMEBankAccount.id !== undefined) {
            this.subscribeToSaveResponse(this.aCMEBankAccountService.update(this.aCMEBankAccount));
        } else {
            this.subscribeToSaveResponse(this.aCMEBankAccountService.create(this.aCMEBankAccount));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IACMEBankAccount>>) {
        result.subscribe((res: HttpResponse<IACMEBankAccount>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get aCMEBankAccount() {
        return this._aCMEBankAccount;
    }

    set aCMEBankAccount(aCMEBankAccount: IACMEBankAccount) {
        this._aCMEBankAccount = aCMEBankAccount;
    }
}
