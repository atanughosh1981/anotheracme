import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IACMEBankAccount } from 'app/shared/model/acme-bank-account.model';

type EntityResponseType = HttpResponse<IACMEBankAccount>;
type EntityArrayResponseType = HttpResponse<IACMEBankAccount[]>;

@Injectable({ providedIn: 'root' })
export class ACMEBankAccountService {
    private resourceUrl = SERVER_API_URL + 'api/acme-bank-accounts';

    constructor(private http: HttpClient) {}

    create(aCMEBankAccount: IACMEBankAccount): Observable<EntityResponseType> {
        return this.http.post<IACMEBankAccount>(this.resourceUrl, aCMEBankAccount, { observe: 'response' });
    }

    update(aCMEBankAccount: IACMEBankAccount): Observable<EntityResponseType> {
        return this.http.put<IACMEBankAccount>(this.resourceUrl, aCMEBankAccount, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IACMEBankAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IACMEBankAccount[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
