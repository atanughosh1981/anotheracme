import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IACMETransaction } from 'app/shared/model/acme-transaction.model';
import { Principal } from 'app/core';
import { ACMETransactionService } from './acme-transaction.service';

@Component({
    selector: 'jhi-acme-transaction',
    templateUrl: './acme-transaction.component.html'
})
export class ACMETransactionComponent implements OnInit, OnDestroy {
    aCMETransactions: IACMETransaction[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private aCMETransactionService: ACMETransactionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.aCMETransactionService.query().subscribe(
            (res: HttpResponse<IACMETransaction[]>) => {
                this.aCMETransactions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInACMETransactions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IACMETransaction) {
        return item.id;
    }

    registerChangeInACMETransactions() {
        this.eventSubscriber = this.eventManager.subscribe('aCMETransactionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
