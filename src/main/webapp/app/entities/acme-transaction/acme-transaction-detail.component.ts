import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IACMETransaction } from 'app/shared/model/acme-transaction.model';

@Component({
    selector: 'jhi-acme-transaction-detail',
    templateUrl: './acme-transaction-detail.component.html'
})
export class ACMETransactionDetailComponent implements OnInit {
    aCMETransaction: IACMETransaction;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ aCMETransaction }) => {
            this.aCMETransaction = aCMETransaction;
        });
    }

    previousState() {
        window.history.back();
    }
}
