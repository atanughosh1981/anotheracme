import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IACMETransaction } from 'app/shared/model/acme-transaction.model';
import { ACMETransactionService } from './acme-transaction.service';

@Component({
    selector: 'jhi-acme-transaction-delete-dialog',
    templateUrl: './acme-transaction-delete-dialog.component.html'
})
export class ACMETransactionDeleteDialogComponent {
    aCMETransaction: IACMETransaction;

    constructor(
        private aCMETransactionService: ACMETransactionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.aCMETransactionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'aCMETransactionListModification',
                content: 'Deleted an aCMETransaction'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-acme-transaction-delete-popup',
    template: ''
})
export class ACMETransactionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ aCMETransaction }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ACMETransactionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.aCMETransaction = aCMETransaction;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
