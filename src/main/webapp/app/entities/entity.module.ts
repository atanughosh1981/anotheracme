import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AcmebankACMEBankAccountModule } from './acme-bank-account/acme-bank-account.module';
import { AcmebankACMETransactionModule } from './acme-transaction/acme-transaction.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        AcmebankACMEBankAccountModule,
        AcmebankACMETransactionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AcmebankEntityModule {}
