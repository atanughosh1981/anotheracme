import { NgModule } from '@angular/core';

import { AcmebankSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [AcmebankSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [AcmebankSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class AcmebankSharedCommonModule {}
