export interface IACMEBankAccount {
    id?: number;
    accountNumber?: string;
    username?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
    address?: string;
    postalCode?: string;
    panNumber?: string;
}

export class ACMEBankAccount implements IACMEBankAccount {
    constructor(
        public id?: number,
        public accountNumber?: string,
        public username?: string,
        public password?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public phone?: string,
        public address?: string,
        public postalCode?: string,
        public panNumber?: string
    ) {}
}
