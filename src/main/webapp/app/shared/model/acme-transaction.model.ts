import { Moment } from 'moment';

export const enum TRANSTYPE {
    DEBIT = 'DEBIT',
    CREDIT = 'CREDIT'
}

export interface IACMETransaction {
    id?: number;
    accountNumber?: string;
    transactionDate?: Moment;
    amount?: number;
    transactionRemark?: string;
    transactionType?: TRANSTYPE;
    availableBalance?: number;
}

export class ACMETransaction implements IACMETransaction {
    constructor(
        public id?: number,
        public accountNumber?: string,
        public transactionDate?: Moment,
        public amount?: number,
        public transactionRemark?: string,
        public transactionType?: TRANSTYPE,
        public availableBalance?: number
    ) {}
}
